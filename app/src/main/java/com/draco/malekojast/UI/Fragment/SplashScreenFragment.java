package com.draco.malekojast.UI.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.draco.malekojast.BuildConfig;
import com.draco.malekojast.R;
import com.draco.malekojast.Utils.Helping;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class SplashScreenFragment extends Fragment {

    @BindView(R.id.ic_free_world)
    ImageView icFreeWorld;
    @BindView(R.id.app_version)
    TextView appVersion;

    public SplashScreenFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_splash_screen, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        appVersion.setText(BuildConfig.VERSION_NAME);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Helping.frameCommint(getFragmentManager(), new DashboardFragment());
            }
        }, 5000);
    }
}
