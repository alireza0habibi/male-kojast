package com.draco.malekojast.UI.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;

import com.draco.malekojast.R;
import com.draco.malekojast.UI.Fragment.SplashScreenFragment;
import com.draco.malekojast.Utils.Helping;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Helping.frameAdd(getSupportFragmentManager(), new SplashScreenFragment());
    }
}
