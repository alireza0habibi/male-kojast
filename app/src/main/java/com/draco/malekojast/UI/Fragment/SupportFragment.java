package com.draco.malekojast.UI.Fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.draco.malekojast.R;
import com.draco.malekojast.UI.Activity.MainActivity;
import com.draco.malekojast.Utils.Helping;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class SupportFragment extends Fragment {

    @BindView(R.id.sendMail)
    TextView sendMail;
    @BindView(R.id.btn_back)
    ImageButton btnBack;

    public SupportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_support, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.sendMail)
    void sendMail() {


        try{
            Intent intent = new Intent (Intent.ACTION_VIEW , Uri.parse("mailto:" + "alireza0habibi@gmail.com"));
            intent.putExtra(Intent.EXTRA_SUBJECT, "انتقادات و پیشنهادات");
            startActivity(intent);
        }catch(ActivityNotFoundException e){
            //TODO smth
        }


    }

    @OnClick(R.id.btn_back)
    void goBack() {
        Helping.frameCommint(getFragmentManager(), new DashboardFragment());
    }
}
