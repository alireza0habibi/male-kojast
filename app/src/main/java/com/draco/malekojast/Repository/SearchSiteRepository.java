package com.draco.malekojast.Repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.draco.malekojast.Models.CountryModel;
import com.draco.malekojast.UI.Fragment.DashboardFragment;
import com.draco.malekojast.res.Api;
import com.draco.malekojast.res.RetrofitSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchSiteRepository {

    public LiveData<CountryModel> searchSites(String website_name) {
        MutableLiveData<CountryModel> mutableLiveData = new MutableLiveData<>();

        RetrofitSingleton.getRetrofit().create(Api.class).searchSite(website_name).enqueue(new Callback<CountryModel>() {
            @Override
            public void onResponse(Call<CountryModel> call, Response<CountryModel> response) {
                if (response.body() != null)
                    if (response.body().getStatus().equals("success")) {
                        mutableLiveData.postValue(response.body());
                    } else if (response.body().getStatus().equals("fail")) {
                        DashboardFragment.self.failRequest("سایت مورد نظر یافت نشد!");
                    }
            }

            @Override
            public void onFailure(Call<CountryModel> call, Throwable t) {

            }
        });

        return mutableLiveData;
    }
}
